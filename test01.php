<?php

define('MY_CONST',123);

echo MY_CONST;
echo '<br>';

echo __DIR__;
echo '<br>';
echo __FILE__. '<br>';
echo __LINE__. '<br>';
echo PHP_VERSION. '<br>';

$a = 55;
$b = 'aaa';
$c = true;

echo '<br>';
echo true?: 'fff';
echo '<br>';
echo false?: 'fff';

$aa =  2 || $bb = 3;
echo '<br>';
echo $aa;
echo $bb;